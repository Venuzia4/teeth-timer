import { Component } from '@angular/core';
import { ViewWillEnter } from '@ionic/angular';
import { Preferences } from '@capacitor/preferences';
import { BehaviorSubject } from 'rxjs';

const circleR = 80;
const circleDasharray = 2 * Math.PI * circleR
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements ViewWillEnter {

  public name:string | null ="mon petit marco";
  public brushingArea!:number;
  public areaChange!:number;
  public waitingTime!:number;


  percent:BehaviorSubject<number>=new BehaviorSubject(100);
  ms:any='0' + 0 ;
  sec:any='0' + 0 ;
  min:any='0' + 0 ;
  hr:any='0' + 0 ;
  circleR = circleR;
  circleDasharray = circleDasharray;
  startTimer:any;
  running:boolean = false;

  async ionViewWillEnter(){
    this.name = await (await Preferences.get({key :'name'})).value;
    this.brushingArea = parseInt((await Preferences.get({key :'brushingArea'})).value!,10);
    this.areaChange = parseInt ((await Preferences.get({key :'areaChange'})).value!,10);
    this.waitingTime = parseInt ((await Preferences.get({key :'waitingTime'})).value!,10);

   }

  start():void{
    if(!this.running){
      let areaLeft = this.brushingArea;
      let timeLeft = this.areaChange;
      let waitTimeLeft = this.waitingTime;

      this.startTimer = setInterval(()=>{
        this.ms++;
        this.ms = this.ms < 10 ?'0' + this.ms : this.ms;

        if(this.ms ===100){
          this.sec++;
          this.sec = this.sec <10 ? '0' + this.sec :this.sec;
          this.ms ='0'+ 0;
        }

        if(this.sec === timeLeft){
          areaLeft--;
          if (areaLeft > 0) {
            this.percent.next((areaLeft / this.brushingArea) * 100);
            clearInterval(this.startTimer);
            timeLeft = this.areaChange;
            waitTimeLeft = this.waitingTime;
            console.log(`Fin de la zone ${this.brushingArea - areaLeft}`);
            console.log(`Pause de ${waitTimeLeft} secondes`);
            let waitTimer = setInterval(() => {
              waitTimeLeft--;
              if (waitTimeLeft <= 0) {
                clearInterval(waitTimer);
                console.log("Crachez et passez à la zone suivante");
                this.start();
              }
            }, 1000);
          } else {
            if(areaLeft==0){
            clearInterval(this.startTimer);
            console.log("Temps de brossage terminé");
          }}
          this.brushingArea = areaLeft;
          this.sec ='0'+ 0;
        }

        if(this.min === 60){
          this.hr++;
          this.hr = this.hr <10 ? '0' + this.hr :this.hr;
          this.min ='0'+ 0;
        }
      },10);

    }else{
      this.stop();
    }
  }


  stop():void{
    clearInterval(this.startTimer);
    this.running = false;
  }

  percentageOffset(percent:any){
    const percentFloat = percent / 100;
    return circleDasharray * (1 - percentFloat);
  }

  reset():void{
    clearInterval(this.startTimer);
    this.running = false;
    this.hr=this.min= this.sec=this.ms= '0' + 0;

  }






}
