import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup,FormControl} from '@angular/forms';
import { Preferences } from '@capacitor/preferences';





@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
 public brushingInfo!:FormGroup;

 constructor(private formBuilder:FormBuilder ){

  this.brushingInfo=this.formBuilder.group({
    name:new FormControl ('mon petit marco',[Validators.required,
    Validators.maxLength(20)]),
    brushingArea:new FormControl ('',[Validators.required,
      Validators.pattern(/^[1-9]\d*$/),Validators.max(15)]),
    areaChange:new FormControl ('',[Validators.required,
      Validators.min(10),Validators.pattern(/^[1-9]\d*$/)]),
    waitingTime:new FormControl ('' ,[Validators.required,Validators.required,
      Validators.pattern(/^[1-9]\d*$/)])

  });
 }


 get nameControl() {
  return this.brushingInfo.get('name');
}


get brushingAreaControl() {
  return this.brushingInfo.get('brushingArea');
}

get areaChangeControl() {
  return this.brushingInfo.get('areaChange');
}
get waitingTimeControl() {
  return this.brushingInfo.get('waitingTime');
}
  setBrunshingInfo = async ()=>{
    const name =this.brushingInfo.value.name;
    const brushingArea =this.brushingInfo.value.brushingArea;
    const areaChange =this.brushingInfo.value.areaChange;
    const waitingTime =this.brushingInfo.value.waitingTime;

    //stringify ici pour passer sur MAC
    await Preferences.set({key:'name',value:name});
    await Preferences.set({key:'brushingArea',value:brushingArea});
    await Preferences.set({key:'areaChange',value:areaChange});
    await Preferences.set({key:'waitingTime',value:waitingTime});


  }



}
