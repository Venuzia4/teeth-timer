import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.simplon.teethtimer',
  appName: 'teethtimer',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
